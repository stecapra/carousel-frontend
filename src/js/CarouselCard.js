"use strict"

import utils from './utils.js'

export default class CarouselCard {  
  constructor (parentId, index, cardWidth) {
    this._parentId = parentId
    this._index = index
    this._cardWidth = cardWidth
    
    this._DOMElements = {
      card: utils.createDOMElement("li", ["carousel-card", "carousel-card-placeholder"]),
      image: utils.createDOMElement("div", ["carousel-card-image", "full-width"]),
      imageOverlay: utils.createDOMElement("div", "carousel-image-overlay"),
      imageOverlayBtn: utils.createDOMElement("i", ["material-icons", "carousel-icon", "carousel-image-overlay-btn", "hidden"]),
      type: utils.createDOMElement("span", "carousel-card-type"),
      duration: utils.createDOMElement("span", "carousel-card-duration"),
      title: utils.createDOMElement("p", ["carousel-card-title", "primary"])
    }
    
    this._createPlaceholder()
    
    this._bindEvents()
  }
  
  getDOMElement() {
    return this._DOMElements.card
  }
  getIndex () {
    return this._index
  }
  
  _createPlaceholder() {
    this._DOMElements.card.style.width = this._cardWidth + "px"
    
    this._DOMElements.imageOverlayBtn.innerHTML = "play_circle_outline"
    this._DOMElements.imageOverlay.appendChild(this._DOMElements.imageOverlayBtn)
    this._DOMElements.image.appendChild(this._DOMElements.imageOverlay)
    
    this._DOMElements.image.appendChild(this._DOMElements.type)
    
    this._DOMElements.image.appendChild(this._DOMElements.duration)
    
    this._DOMElements.card.appendChild(this._DOMElements.image)
    
    var titleContainer = utils.createDOMElement("div", "carousel-card-title-container")
    titleContainer.appendChild(this._DOMElements.title)
    
    this._DOMElements.card.appendChild(titleContainer)
  }
  
  fill({image, type, duration, title, cardinality}) {
    utils.removeClass(this._DOMElements.card, 'carousel-card-placeholder')
    
    if(cardinality !== 'single') utils.addClass(this._DOMElements.card, "carousel-card-multiple")

    this._DOMElements.image.style["background-image"] = "url(./assets/" + image + ")"
    this._DOMElements.type.innerHTML = type
    this._DOMElements.duration.innerHTML = utils.sec2FormattedTime(duration)
    this._DOMElements.title.innerHTML = title
  }
  
  /*
   *  Events section
   */
  _bindEvents() {
    this._DOMElements.image.addEventListener("mouseover", () => this._onImageMouseOver())
    this._DOMElements.image.addEventListener("mouseleave", () => this._onImageMouseLeave())
    
    this._DOMElements.imageOverlayBtn.addEventListener("click", () => this._onImageOverlayBtn())
  }
  
  _onImageMouseOver() {
    if(!utils.hasClass(this._DOMElements.card, "carousel-card-placeholder")) {
      this._DOMElements.imageOverlay.style.height = "100%"
      utils.removeClass(this._DOMElements.imageOverlayBtn, "hidden")
    }
  }
  _onImageMouseLeave() {
     this._DOMElements.imageOverlay.style.height = "0"
      utils.addClass(this._DOMElements.imageOverlayBtn, "hidden")
  }
  
  _onImageOverlayBtn() {
    alert("Clicked video number " + this._index)
  }
}