"use strict"

import CarouselCard from './CarouselCard.js'
import utils from './utils.js'

var chunkSize = utils.isSmartphone() ? 2 : 6
var chunkLoadedInBackground = utils.isSmartphone() ? 2 : Math.floor(chunkSize / 3) //# of cards loaded in background when scrolling right
var maxCards = 15 // only for test purpose. Once a carousel has n cards, the "server" reponds with 0 cards 

window.Carousel = class Carousel {
  constructor({container, icon, title, subtitle, fetchCards}) {   
    this._opt = {
      container,
      icon,
      title,
      subtitle,
      fetchCards
    }
    
    // Basic DOM elements of carousel
    this._DOMElements = {
      root: document.getElementsByTagName("body")[0],
      container: utils.createDOMElement("div"),
      cardsContainer: utils.createDOMElement("div", "carousel-cards-container"),
      cards: utils.createDOMElement("ul", "carousel-cards"),
      leftArrow: utils.createDOMElement("div", ["carousel-arrow", "carousel-arrow-left", "hidden"]),
      rightArrow: utils.createDOMElement("div", ["carousel-arrow", "carousel-arrow-right", "hidden"])
    }
    
    this._cards = []            //  Array of cards objects
    
    this._cardWidth = this._calculateCardWidth()
    
    this._loadNextChunk = true  //  Whether load next chunk of cards. False if server response with 0 cards
    
    this._drag = {} // Values for drag events
    this._resetValueDrag()  // instantiate/reset drag object
    
    this._bootstrap()
  }
  
  // Calculate dinamically cards' width depending of the screen's width.
  _calculateCardWidth() {
    let cardWidth = parseInt(window.getComputedStyle(this._DOMElements.root, null).width)
    cardWidth -= 40  // remove padding around UL tag
    cardWidth -= chunkSize * 10  // remove margin from each card
    return Math.floor(cardWidth / chunkSize)
  }
  
  _bootstrap() {
    this._DOMElements.container.id = this._opt.container
    
    this._addIcon()
    this._addHeader()
    this._addCardsContainer()
    
    this._bindEvents()
    
    this._DOMElements.root.appendChild(this._DOMElements.container)
    
    this._loadCards(chunkSize + chunkLoadedInBackground)
  }
  
  /* 
  * DOM element creation
  */
  _addIcon() {
    var icon = utils.createDOMElement("i", ["material-icons", "carousel-icon", "bkg-primary"])
    icon.innerHTML = this._opt.icon
    this._DOMElements.container.appendChild(icon)
  }
  
  _addHeader() {
    var container = utils.createDOMElement("div", "carousel-header-container")
    
    var title = utils.createDOMElement("span", ["carousel-title", "primary"])
    title.innerHTML = this._opt.title
    
    var subtitle = utils.createDOMElement("span", "carousel-subtitle")
    subtitle.innerHTML = this._opt.subtitle
    
    container.appendChild(title)
    container.appendChild(subtitle)
    this._DOMElements.container.appendChild(container)
  }
  
  _addCardsContainer() {
    this._addCards()
    
    this._addArrows()
    
    this._DOMElements.container.appendChild(this._DOMElements.cardsContainer)
  }
  _addArrows() {
    var arrowLeftSymbol = utils.createDOMElement("span")
    arrowLeftSymbol.innerHTML = "<"
    this._DOMElements.leftArrow.appendChild(arrowLeftSymbol)
    
    var arrowRightSymbol = utils.createDOMElement("span")
    arrowRightSymbol.innerHTML = ">"
    this._DOMElements.rightArrow.appendChild(arrowRightSymbol)
    
    this._DOMElements.cardsContainer.appendChild(this._DOMElements.leftArrow)
    this._DOMElements.cardsContainer.appendChild(this._DOMElements.rightArrow)
  }
  _addCards() {
    this._DOMElements.cards.style.width = window.innerWidth - 30 + "px"
    this._DOMElements.cardsContainer.appendChild(this._DOMElements.cards)
  }
  
  /*
   *  Load cards section
   */

  /*
   *  Possible Test: test whether after _loadCards, cardsContainer contain exactely nCards
   *  If multiple calls, before call the test, need to save the current number of cards to be added after the test
   */
  async _loadCards(nCards) {
    const offset = this._getTotalCardsNumber()
    // Create n placeholder before retrieve data from server 
    this._createCardsPlaceholders(nCards)

    /*
     *  Only for test purpose. remove in production
     *
     *  Remove From here
     */
    let returnNoData = false
    if(this._getTotalCardsNumber() > maxCards) returnNoData = true
    /*
     *  Remove until here
     */
    
    let loadedCards = await this._opt.fetchCards(nCards, returnNoData)
    for(var i = 0; i < nCards; i++) {
      var currentCard = this._cards[offset + i]
      if(loadedCards[i]) currentCard.fill(loadedCards[i]) // fill placeholder with data
      else this._removeCard(currentCard)  // remove placeholder created before
    }
    
    if(loadedCards.length === 0) this._loadNextChunk = false // if no more data is retrieved, stop loading next chunk - rigth arrow and scrolling events -  
        
    return loadedCards
  }
  _createCardsPlaceholders(nCards) {
    const offset = this._getTotalCardsNumber()
    
    for(var i = 0; i < nCards; i++) {
      var card = new CarouselCard(this._opt.id, offset + i, this._cardWidth)
      this._cards.push(card)
      this._DOMElements.cards.appendChild(card.getDOMElement())
    }
  }
  
  _getTotalCardsNumber() {
    return this._DOMElements.cards.getElementsByTagName("LI").length
  }
  
  _removeCard(card) {
    this._DOMElements.cards.removeChild(card.getDOMElement())
    this._cards.splice(card.getIndex(), 0)
  }
  
  /*
   *  Events section
   */ 
  _bindEvents() {
    this._DOMElements.leftArrow.addEventListener("click", () => this._onArrowClicked("left"))
    this._DOMElements.rightArrow.addEventListener("click", () => this._onArrowClicked("right"))
    
    this._DOMElements.cardsContainer.addEventListener("mouseover", () => this._onCardsContainerMouseOver())
    this._DOMElements.cardsContainer.addEventListener("mouseleave", () => this._onCardsContainerMouseLeave())
    
    this._DOMElements.cards.addEventListener('mousedown', e => this._onCardsStartDrag(e))
    this._DOMElements.cards.addEventListener('touchstart', e => this._onCardsStartDrag(e))
    this._DOMElements.cards.addEventListener('mousemove', e => this._onCardsDragMove(e))
    this._DOMElements.cards.addEventListener('touchmove', e => this._onCardsDragMove(e))
    this._DOMElements.root.addEventListener('mouseup', e => this._onCardsDragEnd(e))
    this._DOMElements.root.addEventListener('touchend', e => this._onCardsDragEnd(e))
  }
  
  async _onArrowClicked(direction) {
    await this._animateHorizontalScrolling(direction)
    
    if(direction === "right" && this._loadNextChunk) this._loadCards(chunkLoadedInBackground)
    
    this._checkArrowVisibility()
  }
  
  /* 
   *  Visibily arrow events
   */
  _onCardsContainerMouseOver() {
    this._checkArrowVisibility()
  }
  _onCardsContainerMouseLeave() {
    utils.addClass(this._DOMElements.leftArrow, "hidden")
    utils.addClass(this._DOMElements.rightArrow, "hidden")
  }
  
  /*
   *  Horiontal mouse dragging events
   */
  _onCardsStartDrag(e) {
    this._drag.startDragX = utils.getScreenX(e)
    this._drag.startDragX_test = this._drag.startDragX
  }
  _onCardsDragMove(e) {
    if(this._drag.startDragX > -1) {
      const currentScreenX = utils.getScreenX(e)
      var difference = this._drag.startDragX - currentScreenX 
     
      this._DOMElements.cards.scrollLeft += difference
      
      this._drag.startDragX = currentScreenX
        
      if(this._loadNextChunk && this._isEndScrolling()) this._loadCards(chunkLoadedInBackground)
      
      this._checkDragSpeed()
    }
  }

  _onCardsDragEnd(e) {
    if(this._drag.startDragX > -1) {
      this._drag.startDragX = -1
    
      this._smoothScrolling()
    }
  }

  _resetValueDrag () {
    this.lastPos = null;
    this.delta = 0;
  }

  _checkDragSpeed() {
    this._drag.newPos = this._DOMElements.cards.scrollLeft;
    if(this._drag.lastPos != null ) this._drag.dragSpeed = this._drag.newPos - this._drag.lastPos;
    this._drag.lastPos = this._drag.newPos;
    clearTimeout(this._drag.timer);
    this._drag.timer = setTimeout(this._resetValueDrag(), this._drag.delay);
  }
  
  /*
   *  Utils section
   */
  _checkArrowVisibility() {
    // Check if is the start of the list
    if(this._DOMElements.cards.scrollLeft === 0) utils.addClass(this._DOMElements.leftArrow, "hidden")
    else utils.removeClass(this._DOMElements.leftArrow, "hidden")
    
    // Check if is the end of the list
    if(this._isEndScrolling()) utils.addClass(this._DOMElements.rightArrow, "hidden")
    else utils.removeClass(this._DOMElements.rightArrow, "hidden")
  }
  _isEndScrolling() {
    let maxScroll = this._DOMElements.cards.scrollWidth - this._DOMElements.cards.clientWidth // calculate the max possible scroll 
    maxScroll -= 10 //if last card is hover, it will increase size, therefore maxscroll will be higher. 10 is a safe value to include this case 
    return this._DOMElements.cards.scrollLeft >= maxScroll
  }
  
   /*
   *  Animation section
   */
  _animateHorizontalScrolling(direction) {
    const offset = chunkLoadedInBackground * this._cardWidth    // total pixel to be scrolled
    var step = 15 // velocity of scrolling
    
    return new Promise((resolve, reject) => {
      var scrollAmount = 0;
      
      // Scroll "step" pixel until "scrollAmout" reach the "offset"
      var interval = window.setInterval(() => {
        if(direction == 'left') this._DOMElements.cards.scrollLeft -= step;
        else this._DOMElements.cards.scrollLeft += step;
        scrollAmount += step;

        if(scrollAmount >= offset) {
          clearInterval(interval)
          resolve()
        }
      }, 1)
    })
  }
  _smoothScrolling() {
    if(this._drag.dragSpeed === -1) return    // Case when scroll is ended
    
    // Tune this value for better smoothing effect
    const alpha = 0.1
    const beta = 0.3
    
    let intervalCont = 50 * (this._drag.dragSpeed * alpha)
    if(intervalCont < 0) intervalCont = intervalCont * -1   // negative if this._drag.dragSpeed is negative -> dragging left
    
    const step = 1 * (this._drag.dragSpeed * beta)
    
    let interval = setInterval(() => {
      if(intervalCont < 0 || this._DOMElements.cards.scrollLeft <= 0 || this._isEndScrolling()) clearInterval(interval)
      else {
        this._DOMElements.cards.scrollLeft += step
        intervalCont--;
      }
    }, 1)
  }
}

