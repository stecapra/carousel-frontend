"use strict"

function isSmartphone() {
  return navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
}
function getScreenX(e) {
  if(isSmartphone() && e.touches && e.touches.length > 0 && e.touches[0]) return e.touches[0].screenX 
  return e.screenX
}

function createDOMElement(tag, clazz) {
  var ret = document.createElement(tag)
  if(clazz) addClass(ret, clazz)
  return ret
}
function addClass(elem, clazz) {
  if(!Array.isArray(clazz)) clazz = [clazz]
  for(var c of clazz) {
    if(elem.className.indexOf(clazz) === -1) elem.className = elem.className + " " + c
  }
}
function removeClass(elem, clazz) {
  if(!Array.isArray(clazz)) clazz = [clazz]
  for(var c of clazz) elem.className = elem.className.replace(c, '')
}
function hasClass(elem, clazz) {
  return elem.className.indexOf(clazz) > -1
}
function findChildById(element, id) {
  var children = element.children
  for(var child of children) {
    if(child.id === id) return child
  }
  return null
}
function findChildByTag(element, tag) {
  var children = element.children
  for(var child of children) {
    if(child.tagName === tag) return child
  }
  return null
}

function sec2FormattedTime(sec) {
  var hours = Math.floor(sec / 60 / 60)
  sec -= hours * 60 * 60
  var minutes = Math.floor(sec / 60) % 60
  sec -= minutes * 60
  
  var ret = ""
  if(hours > 0) ret += hours + "h"
  if(minutes > 0) {
    if(hours > 0) ret += " "
    ret += minutes + "m" 
  }
  if(sec > 0) {
    if(hours > 0 || minutes > 0) ret += " "
    ret += sec + "s" 
  }
  
  return ret
}

export default {
  isSmartphone,
  getScreenX,
  createDOMElement,
  addClass,
  removeClass,
  hasClass,
  findChildById,
  findChildByTag,
  sec2FormattedTime
}

