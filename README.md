# How to use:

1. Clone the project
2. Go to project folder with terminal
3. Run `yarn`
4. Run `yarn build`
5. Should create `dist` and `node_modules` folders
6. Go to the browser and test the application